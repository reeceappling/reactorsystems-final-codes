/*
Exam by: Reece Appling
*/

//colored text library
const colors = require('colors');

//Import generic functions and apply them to constants as necessary
let ci=require("./Finalfunctions");
const startNewPart=ci.snp,blank=ci.blk,displayInfo=ci.dI,starLine=ci.sL,solnHeader=ci.sH,colorme=ci.cme,colorOut=ci.cO,spaces=ci.sp(),ct = ci.cO;
const avg=ci.avg,interpolate=ci.interpolate,show=ci.show,set=ci.set,err=ci.err,errmsg=ci.errmsg;
//import color name variables 
var u=ci.c;const green=u.g, cyan=u.c, yellow=u.y, magenta=u.m, white=u.w, red=u.r, w=u.w, y=u.y, r=u.r;
//clear data from now unnecessary varibles
ci = null;u = null;

//function to check if a variable is undefined
function isUndefined(toCheck){return ((typeof toCheck)==="undefined");}

//conversion factors
const MWToBtuPerHr = 3412141.6331279;
const BtuPerHrToMW = 1/MWToBtuPerHr;

function givenRx(){
	//define given properties
	this.nominalQ = 3411*MWToBtuPerHr;
	this.nominalTin = 556;
	this.H = 12;
	this.gamma = 0.974;
	this.P = 2250;
	this.Fq = 2.5;
	this.Fz = 1.5;
	this.S = 0.496/12;	
	this.Do = 0.374/12;
	this.Dp = 0.3225/12;
	this.R = this.Dp/2;
	this.kc = 9.6;
	this.thk = 0.0225/12;
	this.HG = 1000;
	this.G = 2.52*(10**6);
	this.nominalTin = 556;
	this.rodArray = 17*17;	
	this.waterRodsPerAssemb = 25;
	this.NA = 193;
	
	
	
	//properties from steam tables
	this.hg = 1115.96;
	this.hf = 700.946;
	this.hfg = this.hg-this.hf;
	this.Tsat = 652.744;
	this.nominalhin = 0;//-------------------------------------------------------------------
	
	
	
	//calculatable properties
	//find number of fuel rods
	this.nSpaces = this.NA*(this.rodArray);
	this.nRods = this.NA*(this.rodArray-this.waterRodsPerAssemb);
	//find cross-sectional areas
	//--------------------------------------------------------------------------------------------ASSUMPTION - Square lattice!
	this.Ax = (this.S**2)-(Math.PI*(this.Do**2)/4);//----------------------------------------------------
	this.AxTotal = this.Ax*this.nSpaces;//----------------------------------------------------
	//find mass flow rate
	this.mTotal = this.G*this.AxTotal;
	this.m = this.G*this.Ax;
	//find rod circumference (wetted perimeter)
	this.rodCirc = Math.PI*this.Do;
	this.Pw = this.rodCirc;
	//Calculate SA of 1 rod
	this.rodSA = this.rodCirc*this.H;
	//calculate total heat transfer area
	this.hxSA = this.rodSA*this.nRods;
	//find De
	this.De = 4*this.Ax/this.Pw;
	//set lambda, He
	this.lambda = 0.301;
	this.He = this.H+(2*this.lambda);
	
	
	
	//defining functions to be used later
	
	//function to output % power
	this.getPowerPct = ()=>{
		return (100*this.Q/this.nominalQ);
	};
	//fxn to print power %
	this.printPowerPct=()=>{
		console.log("Reactor Power:",this.getPowerPct+"%");
	};
	//function to set power, as well as do some other things
	this.setPower=(toQ)=>{
		this.Q=toQ;//set power
	}
	
	
	
	//fxn to set pct power
	this.setPctPower=(pwrPct)=>{
		this.setPower(this.nominalQ*pwrPct/100);
		return this.getPowerPct();
	};
	//fxn to set inlet enthalpy
	this.sethin=(hin)=>{
		if(typeof hin !=="Number"){
			errmsg("Error! inlet enthalpy must be set to a number! Invalid value: ",hin);
			throw new err("hin",hin);
		}
		this.hin=hin;
		return this.hin;
	};
	//fxn to set inlet temp
	this.setTin=(Tin)=>{
		if(typeof Tin !=="Number"){
			errmsg("Error! inlet Temperature must be set to a number! Invalid value: ",Tin);
			throw new err("Tin",Tin);
		}
		else{
			this.Tin=Tin;
			return this.Tin;
		}
	};
	this.HeCalc = (lambdaTemp) => {return (this.H+(2*lambdaTemp));};
	//function for hot channel heat flux
	this.qpphot = (z)=>{
		return (this.qohot*Math.sin((Math.PI*(z+this.lambda))/(this.He)));
	};
	//function for normal channel heat flux
	this.qpp = (z)=>{
		return (this.qo*Math.sin((Math.PI*(z+this.lambda))/(this.He)));
	};
	//function for C in the W3 correlation
	this.CFunc = (z)=>{
		return (0.44*((1-this.xehot(z))**7.9)/((this.G/(10**6))**1.72));};
	//function for hot channel enthalpy
	this.hhot = (z)=>{
		return this.hin+((this.qohot*this.Do*this.He/(this.mTotal*this.gamma))*(Math.cos(Math.PI*this.lambda/this.He)-Math.cos((Math.PI*(z+this.lambda))/this.He)));
	};
	//Function for hot channel quality
	this.xehot = (z)=>{
		return ((this.hhot(z)-this.hf)/this.hfg);
	};
	//function for F in W3
	this.FFunc = function(z){
		let CF = this.CFunc(z);
		let toIntegr = (zz)=>{
			let lc = z;let CC = CF;
			return this.qpphot(zz)*(Math.E**(0-(CC*(lc-zz))));
		};
		let toOut = CF/(this.qpphot(z)*(1-(Math.E**(0-(CF*z)))));
		return toOut*integral(toIntegr,[0,z],1000);
	};

	this.qcEU = (z)=>{
		let xc = this.xehot(z);
		let eqns = [0,0,0,0,0];
		eqns[0] = ((2.022-(0.0004302*this.P))+((0.1722-(0.0000984*this.P))*(Math.E**(xc*(18.177-(0.004129*this.P))))));
		eqns[1] = ((this.G*((0.1484)-(1.596*xc)+(0.1729*xc*Math.abs(xc)))/(10**6))+1.037);
		eqns[2] = (1.157-(1.596*xc));
		eqns[3] = (0.2664+(0.8357*(Math.E**(0-(3.151*this.De)))));//(*12=converted to inches)-
		//eqns[3] = (0.2664+(0.8357*(Math.E**(0-(3.151*this.De*12)))));//(*12=converted to inches)
		eqns[4] = (0.8258+(0.00079*(this.hf-this.hin)));
		return (eqns[0]*eqns[1]*eqns[2]*eqns[3]*eqns[4]*(10**6));
	};
	this.qppcn = (z)=>{
		return this.qcEU(z)/this.FFunc(z);
	};
	
	this.DNBR = (z) => {
		return (this.qppcn(z)/this.qpphot(z));
	};
	//this.DNBR = (z) => {return (this.qppcn(z)/this.qpp(z));};
	
	//function for fuel thermal conductivity
	this.kf = (T)=>{
		return ((3978.1/(692.6+T))+((6.02366*(10**(-12)))*((T+460)**3)));
	}
	
	this.qppp = (z)=>{
		return this.Do*this.qpphot(z)/((this.Dp/2)**2);
	};
	
}

let rxai = new givenRx();
//Log hfg, nSpaces, nRods, Ax, mChannel, Pw, SArod, hxSA, De
//---------------------------------------------------------------------------------------------------------
console.log("AxTotal (ft^2) = "+rxai.AxTotal);
console.log("Core mass flow rate (lbm/hr) = "+rxai.mTotal);
let rxaii = new givenRx();
let rxaiii = new givenRx();
var rxb;

//function for any arbitrary integrals
function integral (fxn,bounds,parts){
	let dx = (bounds[1]-bounds[0])/parts;
	let summation = 0;let currentVal = 0;let currentX = 0;
	for(var i=0;i<parts;i++){
		currentX = bounds[0]+(i*dx);
		currentVal = fxn(currentX)*dx;
		summation = summation+currentVal;
	}return summation;
}



//Directive function for iterative functions
function iterate(eqOneIn,eqTwoIn,bounds,method,acc){
	let eqOne = eqOneIn;//set temporary function variable
	let eqTwo = eqTwoIn;//set temporary function variable
	if(typeof eqOne!="function"){eqOne=function(x){return eqOneIn;};}//creates an arbitrary function if eq1 is a number instead of a function
	if(typeof eqTwo!="function"){eqTwo=function(x){return eqTwoIn;};}//creates an arbitrary function if eq2 is a number instead of a function
	switch(method){
		case "falsePosition":
			return falsePositionMethod(eqOne,eqTwo,bounds,acc);
			break;
		case "bisection":
			return bisectionMethod(eqOne,eqTwo,bounds,acc);
			break;
		default:
			console.log("Method "+method+" not recognized for iteration, falling back to bisection method!");
			return bisectionMethod(eqOne,eqTwo,bounds,acc);
	}
}

//False position method of finding roots in transcendental equations
function falsePositionMethod(funcOne,funcTwo,boundsOrig,acc){
	return new Promise((accept,reject)=>{
		let compoundFunction = function(x){
			return (funcOne(x)-funcTwo(x));
		};
		let newBounds = boundsOrig,lastxn=0;
		while(true){
			let toGo = newBounds;
			let xn = toGo[0]-(((toGo[1]-toGo[0])*compoundFunction(toGo[0]))/(compoundFunction(toGo[1])-compoundFunction(toGo[0])));
			if(Math.abs(xn-lastxn)<acc){
				accept(xn);
				break;
			}
			if(compoundFunction(xn)==0){
				accept(xn);
				break;
			}
			if(Math.sign(compoundFunction(xn))==NaN){
				reject('ERROR! ITERATION YIELDED NaN!');
			}
			if(compoundFunction(xn)*compoundFunction(toGo[0])>0){
				newBounds[0]=xn;
			}
			else{
				newBounds[1]=xn;
			}
			if(Math.abs(newBounds[0]-newBounds[1])<acc){
				accept(xn);
				break;
			}
			lastxn = xn;
		}
		reject(falsePosition);
	});
}

function bisectionMethod(funcOne,funcTwo,bounds,acc){
	return new Promise((accept,reject)=>{
		let newBounds=bounds;
		let lastxn=bounds[0];
		let compoundFunction = (x)=>{
			return (funcOne(x)-funcTwo(x));
		};
		while(true){
			let midPt = (newBounds[0]+newBounds[1])/2;
			if(compoundFunction(midPt)==0||(Math.abs(midPt-newBounds[0])<acc&&Math.abs(midPt-newBounds[1])<acc)){
				accept(midPt);
				break;
			}
			else{
				lastxn=midPt;
				if(Math.sign(compoundFunction(newBounds[0]))==Math.sign(compoundFunction(midPt))){
					newBounds[0]=midPt;
				}
				else{
					if(Math.sign(compoundFunction(newBounds[1]))==Math.sign(compoundFunction(midPt))){
						newBounds[1]=midPt;
					}
					else{
						reject('ERROR! BISECTION ITERATION YIELDED NaN!');
						break;
					}
				}
			}
		}
	});
}

//False position method of finding roots in transcendental equations SYNCHRONOUSLY
function iterateSYNC(funcOne,funcTwo,boundsOrig,acc){
		let compoundFunction = function(x){return (funcOne(x)-funcTwo(x));};
		let newBounds = boundsOrig,lastxn=0;
		while(true){
			let toGo = newBounds;
			let xn = toGo[0]-(((toGo[1]-toGo[0])*compoundFunction(toGo[0]))/(compoundFunction(toGo[1])-compoundFunction(toGo[0])));
			if(Math.abs(xn-lastxn)<acc){
				return xn;
				break;
			}
			if(compoundFunction(xn)==0){
				return xn;
				break;
			}
			if(Math.sign(compoundFunction(xn))==NaN){
				console.log('ERROR! ITERATION YIELDED NaN!');
			}
			if(compoundFunction(xn)*compoundFunction(toGo[0])>0){
				newBounds[0]=xn;}else{newBounds[1]=xn;
			}
			if(Math.abs(newBounds[0]-newBounds[1])<acc){
				return xn;
				break;
			}
			lastxn = xn;
		}
		console.log("False Position method failed!: "+falsePosition);
}




//=================================================================================================== PART A i START
//part a.i
console.log("Part A.i");

//set unknown values for this part to null
rxai.Tin = null;rxai.hin = null;

rxai.MDNBRLimit = 1.3;
//solve for lambda and He
rxai.lambda = 0.301;
rxai.He = rxai.HeCalc(rxai.lambda);
console.log("Lambda =",rxai.lambda,"ft");
console.log("He =",rxai.He,"ft");
console.log("hfg: "+rxai.hfg);
console.log("nChannels: "+rxai.nSpaces);
console.log("nRods: "+rxai.nRods);
console.log("Achannel: "+rxai.Ax);
console.log("Axcore: "+rxai.AxTotal);
console.log("mCore: "+rxai.mTotal);
console.log("mChannel: "+rxai.G*rxai.Ax);
console.log("Pw: "+rxai.Pw);
console.log("rod SA: "+(rxai.Pw*rxai.H));
console.log("Total heat transfer area: "+rxai.hxSA);
console.log("De: "+rxai.De);
function solveAiForGivenh(hin){
	let rx = rxai;
	rx.hin = hin;
	rx.Q=(rx.hg-rx.hin)*rx.mTotal;//Calculate Q that gives hg
	
	rx.qppavg = rx.Q*rx.gamma/rx.hxSA;//calculate qppavg
	rx.qo = rx.qppavg*rx.Fz;//calculate qo for average channel
	rx.qohot = rx.qppavg*rx.Fq;//calculate max heat flux
	rx.qppavgmax = rx.qppavg*rx.Fz;//calculate qppavgmax
	//heat flux equation for hot channel = rx.qpphot(z)
	//fluid enthalpy equation for hot channel = hhot(z)
	//hot channel quality equation = xehot(z)
	let MDNBounds = [rx.H/2,rx.H];
	let MDx = (MDNBounds[1]-MDNBounds[0])/10;
	let currentMin = 999999;
	let initialDNB = rx.DNBR(MDNBounds[0]);
	let lowest = 9999999;
	let lastHit = MDNBounds[0];
	if(initialDNB<currentMin){
		currentMin=initialDNB;
	}
	while(true){//Possibility of infinite loop :(
		for(var j=0;j<10;j++){
			let toCheck = MDNBounds[0]+(MDx*j);
			let checkVal = rx.DNBR(toCheck);
			if(checkVal<currentMin){
				lowest = toCheck;
				currentMin = checkVal;
			}
		}
		MDNBounds = [lowest-MDx,lowest+MDx];
		MDx = (MDNBounds[1]-MDNBounds[0])/10;
		if(MDx<1/(12*12*1000)){break;}
	}
	return (currentMin);
};

let part = 'i';

//Iterate over enthalpy bounds to find correct value
iterate(solveAiForGivenh,rxai.MDNBRLimit,[400,rxai.hg-1],"falsePosition",1/100).then((aih)=>{
	//Output solutions
	console.log("A.i. Solution Inlet Enthalpy: "+aih+" Btu/lbm");
	console.log("From steam tables, P and the above enthalpy corresponds to");
	console.log("Inlet temperature of 598.622 deg F");
	let aiQ = (rxai.hg-aih)*rxai.mTotal;
	rxai.Q = aiQ;
	console.log("Which corresponds to a reactor power of...");
	console.log(aiQ+" Btu/hr");
	console.log((aiQ*BtuPerHrToMW)+" MW");
	console.log((100*aiQ/rxai.nominalQ)+" %");
	rxai.qppavg = rxai.Q*rxai.gamma/rxai.hxSA;
	console.log("q\"avg : "+rxai.qppavg);
	console.log("qo: "+rxai.qppavg*rxai.Fz);
	console.log("qohot: "+rxai.qppavg*rxai.Fq);
	console.log();console.log();
	startAii();
},(aierr)=>{});
//=================================================================================================== PART A ii START
//part a.ii
function startAii(){
	console.log("Part A.ii");
	//Q value is initially set to 100%
	rxaii.Q = rxaii.nominalQ;
	console.log("Q (Btu/hr):"+rxaii.nominalQ);
	//calculate qppavg
	rxaii.qppavg = rxaii.Q*rxaii.gamma/rxaii.hxSA;
	console.log("q\"avg: "+rxaii.qppavg);
	//calculate qo for average channel
	rxaii.qo = rxaii.qppavg*rxaii.Fz;
	console.log("qo: "+rxaii.qo);
	//calculate max heat flux
	rxaii.qohot = rxaii.qppavg*rxaii.Fq;
	console.log("qohot: "+rxaii.qohot);
	//calculate qppavgmax
	rxaii.qppavgmax = rxaii.qppavg*rxaii.Fz;
	//Calculate max enthalpy(using max Q)
	rxaii.bounds = [0,0];
	rxaii.bounds[1] = rxaii.hg-(rxaii.Q/rxaii.mTotal);
	console.log("Aii inlet enthalpy upper bound: "+rxaii.bounds[1]);
	//calculate lambda, He
	rxaii.lambda = 0.301;rxaii.He = rxaii.HeCalc(rxaii.lambda);
	//function given h which will find DNBR
	let solveAiiForGivenh = (hin)=>{
		let rx = rxaii;
		rx.hin = hin;
		let MDNBounds = [rx.H/2,rx.H];
		let MDx = (MDNBounds[1]-MDNBounds[0])/10;
		let currentMin = 999999;
		let initialDNB = rx.DNBR(MDNBounds[0]);
		let lowest = 9999999;
		let lastHit = MDNBounds[0];
		if(initialDNB<currentMin){currentMin=initialDNB;}
		while(true){
			for(var j=0;j<10;j++){
				let toCheck = MDNBounds[0]+(MDx*j);
				let checkVal = rx.DNBR(toCheck);
				if(checkVal<currentMin){lowest = toCheck;currentMin = checkVal;}
			}
			MDNBounds = [lowest-MDx,lowest+MDx];
			MDx = (MDNBounds[1]-MDNBounds[0])/10;
			if(MDx<1/(12*12*1000)){break;}
		}
		return (currentMin);
	};
	//Iterate over enthalpy bounds to find correct value
	iterate(solveAiiForGivenh,rxai.MDNBRLimit,[400,rxaii.bounds[1]],"falsePosition",1/100).then((aiih)=>{
		console.log("A.ii. Solution Inlet Enthalpy: "+aiih+" Btu/lbm");//Solution is ~
		console.log("From steam tables, P and the above enthalpy corresponds to");
		console.log("Inlet temperature of 652.744 deg F");
		console.log();console.log();
		startAiii();
	},(aierr)=>{});
}








//=================================================================================================== PART A iii START
//part a.iii
function startAiii(){
	//Inlet temp is nominal to start
	rxaiii.hin = 554.698;
	console.log("Aiii inlet enthalpy: "+rxaiii.hin);
	rxaiii.Qtrip = rxaiii.mTotal*(rxaiii.hg-rxaiii.hin);
	console.log("Q such that outlet enthalpy is saturated vapor: "+rxaiii.Qtrip);
	//calculate lambda, He
	rxaiii.lambda = 0.301;
	rxaiii.He = rxaiii.HeCalc(rxaiii.lambda);
	let solveAiiiForGivenh = (hout)=>{
		let rx = rxaiii;
		rx.Q = rx.mTotal*(hout-rx.hin);
		//calculate qppavg
		rx.qppavg = rx.Q*rx.gamma/rx.hxSA;
		//calculate qo for average channel
		rx.qo = rx.qppavg*rx.Fz;
		//calculate max heat flux
		rx.qohot = rx.qppavg*rx.Fq;
		//calculate qppavgmax
		rx.qppavgmax = rx.qppavg*rx.Fz;
		let MDNBounds = [rx.H/2,rx.H];
		let MDx = (MDNBounds[1]-MDNBounds[0])/10;
		let currentMin = 999999;
		let initialDNB = rx.DNBR(MDNBounds[0]);
		let lowest = 9999999;
		let lastHit = MDNBounds[0];
		if(initialDNB<currentMin){currentMin=initialDNB;}
		while(true){
			for(var j=0;j<10;j++){
				let toCheck = MDNBounds[0]+(MDx*j);
				let checkVal = rx.DNBR(toCheck);
				if(checkVal<currentMin){
					lowest = toCheck;
					currentMin = checkVal;
				}
			}
			MDNBounds = [lowest-MDx,lowest+MDx];
			MDx = (MDNBounds[1]-MDNBounds[0])/10;
			if(MDx<1/(12*12*1000)){break;}
		}
		return (currentMin);
	};
	rxaiii.qppavgmaxx=(rxaiii.Qtrip*rxaiii.gamma/rxaiii.hxSA);
	console.log("q\"avg for Qmax: "+rxaiii.qppavgmaxx);
	console.log("q\"o,max: "+(rxaiii.qppavgmaxx*rxaiii.Fz));
	console.log("q\"ohot,max: "+(rxaiii.qppavgmaxx*rxaiii.Fq));
	console.log("MDNBR when outlet is sat vapor: "+solveAiiiForGivenh(rxaiii.hg));
	console.log("Because this is above 1.3, and it hits the other limiting factor, the limiting reactor power is...");
	rxaiii.Q = rxaiii.Qtrip;
	console.log(rxaiii.Q+" Btu/hr");
	console.log(rxaiii.Q*BtuPerHrToMW+" MW");
	console.log((100*rxaiii.Q/rxaiii.nominalQ)+" %");
	console.log();console.log();
	//=================================================================================================== PART B START
	//part b
	rxb = rxaiii;
	rxb.Tin = rxb.nominalTin;
	//Calculate average heat flux
	rxb.qppavg = rxb.Q*rxb.gamma/rxb.hxSA;
	//calculate qo for average channel
	rxb.qo = rxb.qppavg*rxb.Fz;
	//calculate max heat flux
	rxb.qohot = rxb.qppavg*rxb.Fq;
	//calculate hot channel average heat flux
	rxb.qppavghot = rxb.qohot/rxb.Fz;
	//Calculate hot channel outlet enthalpy
	//rxb.hout = rxb.hin+(rxb.Q*rxb.Fq/(rxb.mTotal*rxb.Fz));
	//Calculate core average temp
	console.log("Core average outlet temp=Tsat= "+rxb.Tsat);
	rxb.Tavg = (rxb.Tsat+rxb.Tin)/2; 
	console.log("Core average temp: "+rxb.Tavg);
	//find hc
	rxb.Cp = 1.44381;
	rxb.mu = 0.196793;
	rxb.k = 0.303807;
	console.log("Cp at this temperature: "+rxb.Cp);
	console.log("mu at this temperature: "+rxb.mu);
	console.log("k  at this temperature: "+rxb.k);
	let Pr = 0.93524;
	let Re = rxb.G*rxb.De/rxb.mu;
	let WeisConst = (0.042*(rxb.S/rxb.Do))-0.024;
	rxb.hc = (rxb.k*WeisConst*(Re**0.8)*(Pr**(1/3)))/rxb.De;
	console.log("Pr: "+Pr);
	console.log("Re: "+Re);
	console.log("C: "+WeisConst);
	console.log("hc: "+rxb.hc);
	//Function for hot channel enthalpy
	rxb.hhot = (z)=>{
		return (rxb.hin+((rxb.qohot*rxb.nRods*rxb.Do*rxb.He/(rxb.mTotal*rxb.gamma))*(Math.cos(Math.PI*rxb.lambda/rxb.He)-Math.cos((Math.PI*(z+rxb.lambda))/rxb.He))));
	};
	//Hot channel outlet temp
	console.log("Inlet enthalpy: "+rxb.hin);
	console.log("Sat vapor enthalpy: "+rxb.hg);
	rxb.hhotout = (rxb.hhot(rxb.H));
	console.log("Hot channel outlet enthalpy: "+(rxb.hhot(rxb.H)));
	rxb.Tout = 1035.97;
	console.log("Hot channel outlet temperature: "+rxb.Tout);
	//function for Tinfinity in ranges of saturated,subcooled,and superheated*********************************ASSUMING T is linear with regards to H in each range!!!!!!
	rxb.Tinf = (z)=>{
		let h=rxb.hhot(z);
		if(h>=rxb.hf&&h<=rxb.hg){
			return rxb.Tsat;
		}else{
			if(h<rxb.hf){
				return rxb.Tin+(((h-rxb.hin)/(rxb.hf-rxb.hin))*(rxb.Tsat-rxb.Tin));
			}else{
				return rxb.Tsat+(((h-rxb.hg)/(rxb.hhotout-rxb.hg))*(rxb.Tout-rxb.Tsat));
			}
		}
	};
	//Finding Ho
	rxb.HoFind = (z)=>{
		return rxb.hc*(rxb.Tsat-rxb.Tinf(z));
	};
	rxb.Ho = iterateSYNC(rxb.qpphot,rxb.HoFind,[0,rxb.H],1/(12*100));
	console.log("Ho = "+rxb.Ho+"");
	//Calculate thom coefficient
	rxb.thom = (Math.E**((2*rxb.P)/1260))/(72**2);
	//Function for hNB
	rxb.hNB = (Tw)=>{
		return rxb.thom*(10**6)*(Tw-rxb.Tsat);
	};
	//function for Twall MB and NB
	rxb.TwNB = (z)=>{
		let qppz = rxb.qpphot(z);
		let Tinfz = rxb.Tinf(z);
		let TwFunc = (Tw)=>{
			let qppzz = qppz,Tinfzz=Tinfz;
			return (qppzz*(Tw-Tinfzz))+(rxb.hNB(Tw)*(Tw-rxb.Tsat));
		};
		return iterateSYNC(TwFunc,(x)=>{return 0;},[rxb.Tin,3000],1/10);
	};
	//Find Tw equation which covers all regions
	rxb.Tw = (z)=>{
		if(z>rxb.Ho){
			return rxb.TwNB(z);
		}else{
			return rxb.Tinf(z)+(rxb.qpphot(z)/rxb.hc);
		}
	};
	//Find Ro and Ri
	rxb.Ro = rxb.Do/2;
	rxb.Ri = rxb.Ro-rxb.thk;
	//Create equation for Tsurface
	rxb.Ts = (z)=>{
		return rxb.Tw(z)+(rxb.qpphot(z)*((rxb.Ro/(rxb.Ri*rxb.HG))+(rxb.Ro*Math.log(rxb.Ro/rxb.Ri)/rxb.kc)));
	};
	//Create equation for Tcenterline
	rxb.Tc = (z)=>{
		let Tsz = rxb.Ts(z);
		return Tsz+(rxb.qpphot(z)*rxb.R/(2*rxb.kf(Tsz)));
	};
	//Iterate over all H to find highest Tcenterline (Occurs at core midplane!)
	console.log("Thom m = 2");
	console.log("Thom other variable = "+rxb.thom);
	console.log("Max CenterLine temp occurs at the core modplane (6ft) and is: "+rxb.Tc(rxb.H/2));
}